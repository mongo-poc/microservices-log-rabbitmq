package br.com.thiagogbferreira;

import com.rabbitmq.client.Channel;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MicroServiceLogRabbitMqApplication {

  @Around("execution(* br.com.thiagogbferreira.*..*(org.springframework.amqp.core.Message, com.rabbitmq.client.Channel)) && args(message, channel)")
  public Object logRabbit(ProceedingJoinPoint pjp, Message message, Channel channel) throws Throwable {
    MicroServicesContext context = MicroServicesContext.builder()
        .messageId(message.getMessageProperties().getMessageId())
        .key((String) message.getMessageProperties().getHeaders().get("key"))
        .build();
    message.getMessageProperties().getHeaders().put("context", context);
    return MicroServiceLogApplication.proceed( pjp, context );
  }
}
